package de.mischa.spring.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

/**
 * Created by babanin on 29.09.16.
 */
@Entity
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idbooking;

    @OneToMany
    private List<Ticket> tickets;

    @ManyToOne
    @JoinColumn(name = "userid")
    private User user;

    private Date date;

    private boolean cancel;

    public Booking() {
    }

    public int getIdbooking() {
        return idbooking;
    }

    public void setIdbooking(int idbooking) {
        this.idbooking = idbooking;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isCancel() {
        return cancel;
    }

    public void setCancel(boolean cancel) {
        this.cancel = cancel;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "idbooking=" + idbooking +
                ", tickets=" + tickets +
                ", user=" + user +
                ", date=" + date +
                ", cancel=" + cancel +
                '}';
    }
}
