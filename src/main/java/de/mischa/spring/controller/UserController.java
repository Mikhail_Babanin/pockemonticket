package de.mischa.spring.controller;

import de.mischa.spring.model.User;
import de.mischa.spring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;


@Controller
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)    // link
    public String registration(Model model) {
        model.addAttribute("user", new User());
        return "registration";  // datei
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registred(@Valid User user,
                            BindingResult bindingResult, Model model) {

        /*if (userRepository.findByEmail(user.getEmail()) != null) {
            if (userRepository.save(user) != null) {
                return "home";
            }
        }*/
        if (bindingResult.hasErrors()) {
            return "registration";
        } else {
            user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
            userRepository.save(user);
            return "redirect:/";
        }
        //model.addAttribute(user);
        //return "registration";
    }
}