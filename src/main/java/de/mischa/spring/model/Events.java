package de.mischa.spring.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Events {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idevent;

    @ManyToOne
    @JoinColumn(name = "userid")
    private User user;

    @OneToMany
    private List<Performance> performanceList;
    private String eventtype;
    private String nameevent;
    private String description;
    private String nameperformer;
    public Events() {
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public Integer getIdevent() {
        return idevent;
    }

    public void setIdevent(Integer idevent) {
        this.idevent = idevent;
    }

    public String getEventtype() {
        return eventtype;
    }

    public void setEventtype(String eventtype) {
        this.eventtype = eventtype;
    }

    public String getNameevent() {
        return nameevent;
    }

    public void setNameevent(String nameevent) {
        this.nameevent = nameevent;
    }

    public String getNameperformer() {
        return nameperformer;
    }

    public void setNameperformer(String nameperformer) {
        this.nameperformer = nameperformer;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Performance> getPerformanceList() {
        return performanceList;
    }

    public void setPerformanceList(List<Performance> performanceList) {
        this.performanceList = performanceList;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Events{" +
                "idevent=" + idevent +
                ", user_iduser=" + user +
                ", eventtype='" + eventtype + '\'' +
                ", nameevent='" + nameevent + '\'' +
                ", description='" + description + '\'' +
                ", nameperformer='" + nameperformer + '\'' +
                '}';
    }
}
