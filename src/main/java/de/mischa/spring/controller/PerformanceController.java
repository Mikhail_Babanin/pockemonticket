package de.mischa.spring.controller;

import de.mischa.spring.model.Performance;
import de.mischa.spring.model.Venue;
import de.mischa.spring.repository.EventsRepository;
import de.mischa.spring.repository.PerformanceRepository;
import de.mischa.spring.repository.VenueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

@Controller
public class PerformanceController {

    @Autowired
    private PerformanceRepository performanceRepository;

    @Autowired
    private EventsRepository eventsRepository;

    @Autowired
    private VenueRepository venueRepository;

    @RequestMapping(value = "/newPerformance", method = RequestMethod.GET)
    public String registration(@RequestParam Integer eventid, Model model) {
        Performance performance = new Performance();
        performance.setEvents(eventsRepository.findOne(eventid));
        List<Venue> venues = venueRepository.findAll();
        model.addAttribute("venues", venues);
        model.addAttribute("performance", performance);
        return "performanceForm";
    }

    @RequestMapping(value = "/newPerformance", method = RequestMethod.POST)
    public String registred(@Valid Performance performance,
                            BindingResult bindingResult, Model model) {
        System.out.println(bindingResult);
        if (bindingResult.hasErrors()) {
            return "performanceForm";
        } else {
            performanceRepository.save(performance);
            return "redirect:performanceForm?eventid=" + performance.getEvents().getIdevent();
        }
    }
}
