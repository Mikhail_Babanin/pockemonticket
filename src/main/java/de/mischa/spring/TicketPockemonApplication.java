package de.mischa.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TicketPockemonApplication {

	public static void main(String[] args) {
		SpringApplication.run(TicketPockemonApplication.class, args);
	}
}
