package de.mischa.spring.model;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class Performance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idperformance;  // who is making an event

    @ManyToOne
    @JoinColumn(name = "eventid")
    private Events events;     // football, oper, movie etc...

    @ManyToOne
    @JoinColumn(name = "venueid")
    private Venue venue;     // place of event

    private Date date;

    public Performance() {
    }

    public Integer getIdperformance() {
        return idperformance;
    }

    public void setIdperformance(Integer idperformance) {
        this.idperformance = idperformance;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Events getEvents() {
        return events;
    }

    public void setEvents(Events events) {
        this.events = events;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    @Override
    public String toString() {
        return "Performance{" +
                "idperformance=" + idperformance +
                ", events=" + events +
                ", venue=" + venue +
                ", date=" + date +
                '}';
    }
}
