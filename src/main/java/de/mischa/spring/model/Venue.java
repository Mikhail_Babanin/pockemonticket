package de.mischa.spring.model;

import javax.persistence.*;

@Entity
public class Venue {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idvenue;
    private String placevenue;
    private String indexvenue;
    private String streetvenue;

    @ManyToOne
    private City cityid;

    public Venue() {
    }

    public Integer getIdvenue() {
        return idvenue;
    }

    public void setIdvenue(Integer idvenue) {
        this.idvenue = idvenue;
    }

    public String getPlacevenue() {
        return placevenue;
    }

    public void setPlacevenue(String placevenue) {
        this.placevenue = placevenue;
    }

    public String getIndexvenue() {
        return indexvenue;
    }

    public void setIndexvenue(String indexvenue) {
        this.indexvenue = indexvenue;
    }

    public String getStreetvenue() {
        return streetvenue;
    }

    public void setStreetvenue(String streetvenue) {
        this.streetvenue = streetvenue;
    }

    public City getCityid() {
        return cityid;
    }

    public void setCityid(City cityid) {
        this.cityid = cityid;
    }

    /*@Override
    public String toString() {
        return "Venue{" +
                "idvenue=" + idvenue +
                ", placevenue='" + placevenue + '\'' +
                ", indexvenue='" + indexvenue + '\'' +
                ", streetvenue='" + streetvenue + '\'' +
                ", cityid=" + cityid +
                '}';
    }*/
}