package de.mischa.spring.model;

import javax.persistence.*;

@Entity
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idcategory;
    @ManyToOne
    @JoinColumn(name = "performanceid")
    private Performance performanceid;
    private String category;
    private Integer seatnumber;
    private Double price;

    public Category() {
    }

    public Integer getIdcategory() {
        return idcategory;
    }

    public void setIdcategory(Integer idcategory) {
        this.idcategory = idcategory;
    }

    public Performance getPerformanceid() {
        return performanceid;
    }

    public void setPerformanceid(Performance performanceid) {
        this.performanceid = performanceid;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getSeatnumber() {
        return seatnumber;
    }

    public void setSeatnumber(Integer seatnumber) {
        this.seatnumber = seatnumber;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Category{" +
                "idcategory=" + idcategory +
                ", performanceid=" + performanceid +
                ", category='" + category + '\'' +
                ", seatnumber=" + seatnumber +
                ", price=" + price +
                '}';
    }
}
