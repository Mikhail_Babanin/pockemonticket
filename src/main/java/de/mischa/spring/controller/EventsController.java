package de.mischa.spring.controller;

import de.mischa.spring.model.Events;
import de.mischa.spring.repository.EventsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class EventsController {

    @Autowired
    private EventsRepository eventsRepository;

    @RequestMapping(value = "/newEvent", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("events", new Events());
        return "eventsForm";
    }

    @RequestMapping(value = "/newEvent", method = RequestMethod.POST)
    public String registred(@Valid Events events,
                            BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            return "eventsForm";
        } else {
            eventsRepository.save(events);
            return "redirect:/newPerformance?eventid=" + events.getIdevent();
        }
    }
}
