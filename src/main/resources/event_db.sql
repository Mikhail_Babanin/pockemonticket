-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`City`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`City` (
  `idCity` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `countryCode` VARCHAR(45) NOT NULL,
  `district` VARCHAR(45) NOT NULL,
  `population` VARCHAR(45) NOT NULL,
  `City_idCity` INT(11) NOT NULL,
  `Venue_idVanue` INT NOT NULL,
  PRIMARY KEY (`idCity`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Venue`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Venue` (
  `idVenue` INT NOT NULL AUTO_INCREMENT,
  `placeVenue` VARCHAR(45) NOT NULL,
  `indexVenue` VARCHAR(45) NOT NULL,
  `streetVenue` VARCHAR(45) NOT NULL,
  `cityid` INT(11) NOT NULL,
  PRIMARY KEY (`idVenue`),
  INDEX `fk_Venue_City1_idx` (`cityid` ASC),
  CONSTRAINT `fk_Venue_City1`
    FOREIGN KEY (`cityid`)
    REFERENCES `mydb`.`City` (`idCity`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Events`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Events` (
  `idEvent` INT NOT NULL AUTO_INCREMENT,
  `user-idUser` INT NOT NULL,
  `eventType` VARCHAR(45) NOT NULL,
  `nameEvent` VARCHAR(45) NOT NULL,
  `namePerformer` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idEvent`),
  CONSTRAINT `fk_Events_2`
    FOREIGN KEY (`idEvent`)
    REFERENCES `mydb`.`User` (`idUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Performance`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Performance` (
  `idPerformance` INT NOT NULL AUTO_INCREMENT,
  `date` VARCHAR(45) NOT NULL,
  `event_idEvent` INT NOT NULL,
  `venue_idVenue` INT NOT NULL,
  INDEX `fk_Performance_1_idx` (`venue_idVenue` ASC),
  PRIMARY KEY (`idPerformance`),
  INDEX `fk_Performance_2_idx` (`event_idEvent` ASC),
  CONSTRAINT `fk_Performance_1`
    FOREIGN KEY (`venue_idVenue`)
    REFERENCES `mydb`.`Venue` (`idVenue`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Performance_2`
    FOREIGN KEY (`event_idEvent`)
    REFERENCES `mydb`.`Events` (`idEvent`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Category` (
  `idCategory` INT(11) NOT NULL AUTO_INCREMENT,
  `performance_idPerformance` INT(11) NOT NULL,
  `category` CHAR(1) NOT NULL,
  `seatNumber` INT(11) NOT NULL,
  `price` DECIMAL NOT NULL,
  PRIMARY KEY (`idCategory`),
  INDEX `fk_Category_1_idx` (`performance_idPerformance` ASC),
  CONSTRAINT `fk_Category_1`
    FOREIGN KEY (`performance_idPerformance`)
    REFERENCES `mydb`.`Performance` (`idPerformance`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Ticket`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Ticket` (
  `idTicket` INT NOT NULL AUTO_INCREMENT,
  `user_idUser` INT NOT NULL,
  `category_idCategory` INT NOT NULL,
  PRIMARY KEY (`idTicket`),
  INDEX `fk_Ticket_1_idx` (`category_idCategory` ASC),
  INDEX `fk_Ticket_2_idx` (`user_idUser` ASC),
  CONSTRAINT `fk_Ticket_1`
    FOREIGN KEY (`category_idCategory`)
    REFERENCES `mydb`.`Category` (`idCategory`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ticket_2`
    FOREIGN KEY (`user_idUser`)
    REFERENCES `mydb`.`User` (`idUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`User` (
  `idUser` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `street` VARCHAR(45) NOT NULL,
  `index` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idUser`),
  CONSTRAINT `fk_User_2`
    FOREIGN KEY (`idUser`)
    REFERENCES `mydb`.`Ticket` (`idTicket`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_1`
    FOREIGN KEY (`idUser`)
    REFERENCES `mydb`.`City` (`idCity`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Account` (
  `idAccount` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `User_idUser` INT NOT NULL,
  PRIMARY KEY (`idAccount`),
  INDEX `fk_Account_User1_idx` (`User_idUser` ASC),
  CONSTRAINT `fk_Account_User1`
    FOREIGN KEY (`User_idUser`)
    REFERENCES `mydb`.`User` (`idUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

