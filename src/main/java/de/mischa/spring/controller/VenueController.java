package de.mischa.spring.controller;

import de.mischa.spring.model.Venue;
import de.mischa.spring.repository.VenueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class VenueController {

    @Autowired
    private VenueRepository venueRepository;

    @RequestMapping(value = "/newVenue", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("venue", new Venue());
        return "venueForm";
    }

    @RequestMapping(value = "/newVenue", method = RequestMethod.POST)
    public String registred(@Valid Venue venue,
                            BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            return "venueForm";
        } else {
            venueRepository.save(venue);
            return "redirect:/newVenue";
        }
    }
}
